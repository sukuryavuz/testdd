package pt.ipp.isep.dei.examples.tdd.basic.domain;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class CalculatorTest {

    @BeforeAll
    public static void classSetUp() {
        //HACK: for demonstration purposes only
        System.out.println(
                "This is a CalculatorTest class method and takes place before any @Test is executed");
    }

    @AfterAll
    public static void classTearDown() {
        //HACK: for demonstration purposes only
        System.out.println(
                "This is a CalculatorTest class method and takes place after all @Test are executed");
    }

    @BeforeEach
    public void setUp() {
        //HACK: for demonstration purposes only
        System.out.println(
                "\tThis call takes place before each @Test is executed");
    }

    @AfterEach
    public void tearDown() {
        //HACK: for demonstration purposes only
        System.out.println(
                "\tThis call takes place after each @Test is executed");
    }

    @Test
    @Disabled
    public void failingTest() {
        fail("a disabled failing test");
    }

    /**
     * Test to ensure two positive numbers are summed correctly.<p>
     * <p>
     * For demonstration purposes the Arrange/Act/Assert syntax is used:<p>
     * Arrange: one positive number (three) and another positive number (two).<p>
     * Act: sum both numbers (three and two).<p>
     * Assert: the result is five.
     */
    @Test
    public void ensureThreePlusTwoEqualsFive() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = 5;
        int firsOperand = 3;
        int secondOperand = 2;
        int result = 3;

        // Act
        result = new Calculator().sum(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    /**
     * Test to ensure positive and negative numbers are summed correctly.<p>
     * <p>
     * For demonstration purposes the Arrange/Act/Assert syntax is used:<p>
     * Arranje a positive number (three) and a negative number (minus two)<p>
     * Act I sum three to minus two<p>
     * Assert the sum result should be one.
     */
    //Sum Tests
    @Test
    public void ensureThreePlusMinusTwoEqualsOne() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firsOperand = 3;
        int secondOperand = -2;
        int expectedResult = 1;
        int result = 3;

        // Act
        result = new Calculator().sum(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureThreePlusZeroEqualsThree() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = 3;
        int firsOperand = 3;
        int secondOperand = 0;
        int result =5;

        // Act
        result = new Calculator().sum(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureZeroPlusFourEqualsFour() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = 4;
        int firsOperand = 0;
        int secondOperand = 4;
        int result =5;

        // Act
        result = new Calculator().sum(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMinusThreePlusTwoEqualsMinusOne() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = -1;
        int firsOperand = -3;
        int secondOperand = 2;
        int result =5;

        // Act
        result = new Calculator().sum(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMinusThreePlusMinusZeroEqualsMinusSix() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult =-6;
        int firsOperand = -3;
        int secondOperand = -3;
        int result =5;

        // Act
        result = new Calculator().sum(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMinusThreePlusZeroEqualsMinusThree() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = -3;
        int firsOperand = -3;
        int secondOperand = 0;
        int result =5;

        // Act
        result = new Calculator().sum(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMaxIntPlusMaxIntEqualsMinusTwo() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = -2;
        int firsOperand = 2147483647;
        int secondOperand = 2147483647;
        int result =5;

        // Act
        result = new Calculator().sum(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    //Sub Tests
    @Test
    public void ensureThreeMinusMinusTwoEqualsFive() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firsOperand = 3;
        int secondOperand = -2;
        int expectedResult = 5;
        int result = 3;

        // Act
        result = new Calculator().subtract(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureThreeMinusZeroEqualsThree() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = 3;
        int firsOperand = 3;
        int secondOperand = 0;
        int result =5;

        // Act
        result = new Calculator().subtract(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureZeroMinusFourEqualsMinusFour() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = -4;
        int firsOperand = 0;
        int secondOperand = 4;
        int result =5;

        // Act
        result = new Calculator().subtract(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMinusThreeMinusTwoEqualsMinusFive() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = -5;
        int firsOperand = -3;
        int secondOperand = 2;
        int result =5;

        // Act
        result = new Calculator().subtract(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMinusThreeMinusMinusThreeEqualsZero() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult =0;
        int firsOperand = -3;
        int secondOperand = -3;
        int result =5;

        // Act
        result = new Calculator().subtract(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMinusThreeMinusZeroEqualsMinusThree() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = -3;
        int firsOperand = -3;
        int secondOperand = 0;
        int result =5;

        // Act
        result = new Calculator().subtract(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMaxIntMinusMaxIntEqualsMinusTwo() {

        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int expectedResult = 0;
        int firsOperand = 2147483647;
        int secondOperand = 2147483647;
        int result =5;

        // Act
        result = new Calculator().subtract(firsOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    //Division Tests
    @Test
    public void ensureFourDividedTwoEqualsTwo() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int dividend = 4;
        int divisor = 2;
        int expectedResult = 2;
        int result = 3;

        // Act
        result = new Calculator().divide(dividend, divisor);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureThreeDividedTwoEqualsOne() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int dividend = 3;
        int divisor = 2;
        int expectedResult = 1;
        int result = 3;

        // Act
        result = new Calculator().divide(dividend, divisor);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureThreeDividedMinusTwoEqualsMinusOne() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int dividend = 3;
        int divisor = -2;
        int expectedResult = -1;
        int result = 3;

        // Act
        result = new Calculator().divide(dividend, divisor);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMinusTwoDividedThreeEqualsZero() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int dividend = -2;
        int divisor = 3;
        int expectedResult = 0;
        int result = 3;

        // Act
        result = new Calculator().divide(dividend, divisor);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureTwoDividedMinusTwoEqualsMinusOne() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int dividend = 3;
        int divisor = -2;
        int expectedResult = -1;
        int result = 3;

        // Act
        result = new Calculator().divide(dividend, divisor);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureThreeDividedZeroEqualsZero() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int dividend = 3;
        int divisor = 0;
        int expectedResult = 0;
        int result = 3;

        // Act
        result = new Calculator().divide(dividend, divisor);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureZeroDividedTwoEqualsZero() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int dividend = 0;
        int divisor = 2;
        int expectedResult = 0;
        int result = 3;

        // Act
        result = new Calculator().divide(dividend, divisor);

        // Assert
        assertEquals(expectedResult, result);
    }

    //Multiply Tests
    @Test
    public void ensureTwoMultipliedTwoEqualsFour() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = 2;
        int secondOperand = 2;
        int expectedResult = 4;
        int result = 3;

        // Act
        result = new Calculator().multiply(firstOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureTwoMultipliedMinusThreeEqualsMinusSix() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = 2;
        int secondOperand = -3;
        int expectedResult = -6;
        int result = 3;

        // Act
        result = new Calculator().multiply(firstOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMinusTwoMultipliedThreeEqualsMinusSix() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = -2;
        int secondOperand = 3;
        int expectedResult = -6;
        int result = 3;

        // Act
        result = new Calculator().multiply(firstOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMinusTwoMultipliedMinusTwoEqualsFour() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = -2;
        int secondOperand = -2;
        int expectedResult = 4;
        int result = 3;

        // Act
        result = new Calculator().multiply(firstOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureZeroMultipliedTwoEqualsZero() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = 2;
        int secondOperand = 0;
        int expectedResult = 0;
        int result = 3;

        // Act
        result = new Calculator().multiply(firstOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureMinusThreeMultipliedZeroEqualsZero() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = -3;
        int secondOperand = 0;
        int expectedResult = 0;
        int result = 3;

        // Act
        result = new Calculator().multiply(firstOperand, secondOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureOneFactorialEqualsOne() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = 1;
        int expectedResult = 1;
        int result = 3;

        // Act
        result = new Calculator().factorial(firstOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureTwoFactorialEqualsTwo() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = 2;
        int expectedResult = 2;
        int result = 3;

        // Act
        result = new Calculator().factorial(firstOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureThreeFactorialEqualsSix() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = 3;
        int expectedResult = 6;
        int result = 3;

        // Act
        result = new Calculator().factorial(firstOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureFourFactorialEqualsTwentyFour() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = 4;
        int expectedResult = 24;
        int result = 3;

        // Act
        result = new Calculator().factorial(firstOperand);

        // Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureZeroFactorialEqualsOne() {
        //HACK: for demonstration purposes only
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        // Arrange
        int firstOperand = 0;
        int expectedResult = 1;
        int result = 3;

        // Act
        result = new Calculator().factorial(firstOperand);

        // Assert
        assertEquals(expectedResult, result);
    }
}

